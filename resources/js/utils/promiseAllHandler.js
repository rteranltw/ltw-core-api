export const hasPromiseAllErrors = (promiseAllResponse) =>  {

    let numRejectedPromises = promiseAllResponse.filter(response => response.status === 'rejected');
    return (numRejectedPromises.length > 0)? true : false;
    //return (getRejectedPromiseAll(promiseAllResponse).length > 0)? true : false; //this works

};

export const getFulfilledPromiseAll = (promiseAllResponse) =>  {

    return promiseAllResponse.filter(response => response.status === 'fulfilled');

};

export const getRejectedPromiseAll = (promiseAllResponse) =>  {

    return promiseAllResponse.filter(response => response.status === 'rejected');

};
