import 'core-js';
import './load-client-scripts';
import './axios-config';

import Vue from 'vue';
import App from './App.vue';
import router from '@/router';

import ApiService from '@/services/api.service';
import interceptorsSetup from './common/interceptors';

import './global.js';

export function createApp() {

    ApiService.init();
    interceptorsSetup();

    const app = new Vue({
        router,
        render: h => h(App)
    });

    return {
        app
    };

}

/**
 * Init App
 *
 * $mount allows you to explicitly mount the Vue instance when you need to.
 * This means that you can delay the mounting of your vue instance until a
 * particular element exists in your page or some async process has finished,
 * which can be particularly useful when adding vue to legacy apps which inject elements into the DOM
 */
if (document.getElementById('app') !== null) {
    const { app } = createApp();
    app.$mount('#app');
}
