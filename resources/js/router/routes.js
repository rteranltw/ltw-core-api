export default [

    /*{
        path: '',
        name: 'dashboard',
        component: () => import('@/views/Dashboard'),
    },

    {
        path: 'users',
        name: 'user-list',
        component: () => import('@/views/User/UserList'),
    },*/

    {
        name: 'login',
        path: '/backend/login',
        meta: {
            layout: 'clean'
        },
        component: () => import('@/views/Login'),
    },

   {
        path: '/backend/',
       // create a container component
       //https://github.com/vuejs/vue-router/issues/745
       //https://forum.vuejs.org/t/vue-router-route-prefix-or-beforeenter-without-component/8426/2
       component: {
           render (c) { return c('router-view') }
       },
       children: [
            {
                path: '',
                name: 'dashboard',
                component: () => import('@/views/Dashboard'),
            },

            {
                path: 'users',
                name: 'user-list',
                component: () => import('@/views/User/UserList'),
            }
        ]
    }

];
