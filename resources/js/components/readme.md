### Smart vs. Dumb components

It is important to keep a separation between your smart components (Pages folder) from the dumbs (Components folder). In a nutshell:

smart components: can access the store, router, window object...
dumbs components: take props, emits events. That's it!
