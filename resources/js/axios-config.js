import axios from 'axios'

/**
 * Axios config
 */

let token = document.head.querySelector('meta[name="csrf-token"]')

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
}

export { axios }
