import axios from 'axios';
import Vue from 'vue';
//import store from 'your/store/path/store'

export default function setup() {

    // Default -> Add a request interceptor
    axios.interceptors.request.use(function (config) {
        return config;
    }, function (error) {
        return Promise.reject(error);
    });

    // Custom Error response Handler
    function errorResponseHandler(error) {

        //System error notify
        if(error.response.status !== 400 && error.response.data !== undefined && error.response.data.error !== undefined) {
            Vue.toasted.error(error.response.data.error.message);
        }

        return Promise.reject(error);
    }
    axios.interceptors.response.use(
        response => response,
        errorResponseHandler
    );

}
