import Vue from 'vue';

/* ------------------------Vue Global Config------------------------------ */
Vue.config.productionTip = false;


/* ------------------------Vue Global Components------------------------------ */
import DefaultLayout from './layouts/DefaultLayout.vue';
Vue.component('default-layout', DefaultLayout);

import CleanLayout from './layouts/CleanLayout.vue';
Vue.component('clean-layout', CleanLayout);
