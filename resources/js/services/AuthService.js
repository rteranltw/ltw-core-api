import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import { BASE_URL } from '@/common/config';

export default {
    login(username, password) {
        var headers = {
            'Content-Type': 'application/json',
        }
        return Vue.axios.post(`${BASE_URL}/login`, {
            email: username,
            password: password
        }, {headers: headers});
    },
    logout() {
        //return ApiService.get('users', id);
    },
};
