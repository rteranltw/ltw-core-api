import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import { API_URL } from '@/common/config';

export default {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
        Vue.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    },

    query(resource, params) {
        return Vue.axios.get(resource, params).catch(error => {
            throw new Error(`[LTW] ApiService ${error}`);
        });
    },

    get(resource, id = '') {
        return Vue.axios.get(`${resource}/${id}`).catch(error => {
            throw new Error(`[LTW] ApiService ${error}`);
        });
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params);
    },
}
