import ApiService from './api.service';

export default {
    query(params) {
        return ApiService.query('users', {
            params: params
        });
    },
    get(id) {
        return ApiService.get('users', id);
    },
};
