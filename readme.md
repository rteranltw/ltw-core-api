### Steps (only informative)
This steps have been already applied to this repository
- Download laravel (composer create-project --prefer-dist laravel/laravel ltw-core-api)
- Install Passport php (composer require laravel/passport)
- Run command to install passport tables (php artisan migrate)
- Run command to set up the default tokens (php artisan passport:install)
- Config Auth.php -> [guards][api] = 'passport'
- Add trait to User model (use HasApiTokens)
- Add the CreateFreshApiToken middleware to your web middleware group (\Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,)
- To allow ajax request from other domains we must config CORS (composer require barryvdh/laravel-cors)
- Add \Barryvdh\Cors\HandleCors::class middleare to the api group
