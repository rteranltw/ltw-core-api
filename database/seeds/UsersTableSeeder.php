<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'rafa',
            'email' => 'rafa@yopmail.com',
            'email_verified_at' => now(),
            'password' => \Illuminate\Support\Facades\Hash::make('demo'), // password
            'remember_token' => Str::random(10),
        ]);
        factory(App\User::class, 10)->create();
    }
}
